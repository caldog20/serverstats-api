#!/bin/bash

COMPOSE=docker-compose-dev.yml

while getopts "hp" opt; do
  case $opt in
    h)
      echo -e "Usage: $0 [options]\n"
      echo -e "Builds microservices.\n"
      echo -e "\tOptions:"
      echo -e "\t\t-h    Shows information about usage."
      echo -e "\t\t-p    Builds microservices for production environment."
      exit 0
      ;;
    p)
      COMPOSE=docker-compose-prod.yml
      ;;
    \?)
      echo "Use '-h' flag for information about usage"
      exit 1
      ;;
  esac
done
shift "$(( OPTIND - 1 ))"

# Set variables to speed up build process
export COMPOSE_DOCKER_CLI_BUILD=1 \
       DOCKER_BUILDKIT=1

docker-compose -f $COMPOSE build
