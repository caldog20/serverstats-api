#!/bin/bash

COMPOSE=docker-compose-dev.yml
RESTART=false
PROD=false
TEST=false

while getopts "hprt" opt; do
  case $opt in
    h)
      echo -e "Usage: $0 [options]\n"
      echo -e "Builds and starts microservices.\n"
      echo -e "\tOptions:"
      echo -e "\t\t-h    Shows information about usage."
      echo -e "\t\t-p    Runs the system in production environment."
      echo -e "\t\t-t    Only run system for testing."
      echo -e "\t\t-r    Restart the database before starting the system."
      echo -e "\t\t\t--NOTE: This will remove stored data--"
      exit 0
      ;;
    p)
      # PRODUCTION
      COMPOSE=docker-compose-prod.yml
      PROD=true
      ;;
    t)
      # PRODUCTION
      COMPOSE=docker-compose-test.yml
      TEST=true
      ;;
    r)
      # RESTART DATABASE
      RESTART=true
      ;;
    \?)
      echo "Use '-h' flag for information about usage"
      exit 1
      ;;
  esac
done
shift "$(( OPTIND - 1 ))"

if [ "$RESTART" = true ]; then
  docker-compose -f $COMPOSE rm -sf
  docker volume rm -f "$(basename "$PWD")_pgdata"
fi

# If .env exists, read and import its environment variables
if test -f .env; then export $(xargs < .env); fi

# Set variables to speed up build process
export COMPOSE_DOCKER_CLI_BUILD=1 \
       DOCKER_BUILDKIT=1

function on_exit() {
  if [ $? -ne 0 ]; then
    docker-compose -f $COMPOSE rm -sf;
  fi
}

# If exit-signal is caught, run command
trap on_exit EXIT

# If a command fails (non-zero exit code), immediately exit shell
set -e

docker-compose -f $COMPOSE up -d --build pg_service

# Wait for postgres to start
until docker-compose -f $COMPOSE exec -T pg_service pg_isready; do
  echo "pg_service is not up yet, trying to connect...";
  sleep 0.1;
done

if [ "$PROD" = false ] && [ "$TEST" = false ];
then
  docker-compose -f $COMPOSE up -d adminer
fi

docker-compose -f $COMPOSE up --build serverstats_service

# Close database service after running tests
if [ "$PROD" = false ] && [ "$TEST" = true ];
then
  docker-compose -f $COMPOSE down 
fi
