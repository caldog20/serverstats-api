#!/bin/bash

COMPOSE=docker-compose-dev.yml

while getopts "p" opt; do
  case $opt in
    h)
      echo -e "Usage: $0 [options]\n"
      echo -e "Shuts down microservices.\n"
      echo -e "\tOptions:"
      echo -e "\t\t-h    Shows information about usage."
      echo -e "\t\t-p    Shuts down microservices in production environment."
      exit 0
      ;;
    p)
      COMPOSE=docker-compose-prod.yml
      ;;
    \?)
      echo "Use '-h' flag for information about usage"
      exit 1
      ;;
  esac
done
shift "$(( OPTIND - 1 ))"

docker-compose -f $COMPOSE down 

