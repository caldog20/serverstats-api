# Game-Server Master List Server  
## This project is a WIP ##
Depends are currently
``` 
rust nightly

Rocket 0.5.0-rc.1

[dependencies]
rocket = { version = "0.5.0-rc.1", features = ["json"] }
diesel = { version = "1.3", features = ["postgres", "r2d2", "chrono"] }
diesel_migrations = "1.3"
chrono = { version = "0.4", features = ["serde"]}
r2d2 = "0.8"
tokio = { version = "1", features = ["rt-multi-thread", "time"] }


[dependencies.rocket_sync_db_pools]
version = "0.1.0-rc.1"
default-features = false
features = ["diesel_postgres_pool"]

```
### Intended Functionalty 
The function of this project is to provide a service for game clients/web frontends/etc to query for an updated list of currently active game servers with detailed statistics. This can be queried from the game client itself to provide an in-game UI Server List with server details.


## Server
The server should have a few primary functions:
1) Handle queries from clients to provide server listings
2) Handle API calls from game servers to populate the datebase and update existing server values
3) Perform housekeeping on the database to ensure stale entries are removed if not renewed
4) Provide authentication method/whitelisting methods for trusted servers



## Todos
- ADD SSL/TLS!!!
- Rewrite current tests to conform to current codebase and write tests for code intended to be implemented (Sometimes writing tests first may help determine a better way to implement functionality)
- Restructure Server type/table to handle all possible fields. (location, plugins, version, etc)
- Determine what server fields are required/optional
- Implement 'TTL/timeout' functionality to ensure stale entries are cleaned up [WIP]
- Implement authentication or whitelisting for Official Servers
- Determine if server side filtering is needed or if leaving this to the client side is more efficient
- Verify error handling and add more descriptive/type specific errors (diesel/rocket error types with http codes)
- Determine the best abstract implementation to allow game clients/servers, web frontends, etc to efficiently query data
- If this project is desired and continues, write proper documentation for the project and possibly implement client side libs for queries 
- Decide best database implementation to use
- Add pagination for requests
- more things...

[HackMD README]

Server Design discussion link: [Server Design]

[HackMD README]: https://hackmd.io/@zmeKeftTQGSg-jFRu4IbbA/BJj1bl26_
[Server Design]: https://hackmd.io/@zmeKeftTQGSg-jFRu4IbbA/HkBflea6_
