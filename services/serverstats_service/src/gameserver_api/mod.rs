#![allow(unknown_lints)]
#![allow(clippy::nonstandard_macro_braces)]

use crate::gameserver_database;
use crate::models::{Server, UpdateServer};
use rocket::fairing::AdHoc;
use rocket::response::{
    status::{Accepted, Created},
    Debug,
};
use rocket::http::Status;
use rocket::serde::json::Json;

type Result<T, E = Debug<diesel::result::Error>> = std::result::Result<T, E>;

// Get a full list of all Servers with fields
#[get("/all?<filter..>")]
async fn all(db: gameserver_database::Db, filter: gameserver_database::QueryParameters) -> Result<Json<(Vec<Server>, i64)>> {
    let (game_servers, pages): (Vec<Server>, i64) = gameserver_database::all(db, filter).await?;
    Ok(Json((game_servers, pages)))
}

// Update fields on a server already existing in the database
// TODO: Vefiy proper result return on errors
#[put("/<id>", format = "application/json", data = "<server>")]
async fn update(
    db: gameserver_database::Db,
    id: i32,
    server: Json<UpdateServer>,
) -> Result<Accepted<Json<UpdateServer>>> {
    let server_value = server.clone();
    gameserver_database::update(db, id, server_value).await?;
    Ok(Accepted(Some(server)))
}

// Get a specific server with fields
#[get("/<id>")]
async fn read(db: gameserver_database::Db, id: i32) -> Option<Json<Server>> {
    gameserver_database::read(db, id).await.map(Json).ok()
}

// Delete a specific server by ID
#[delete("/<id>")]
async fn delete(db: gameserver_database::Db, id: i32) -> Result<Option<()>> {
    let affected = gameserver_database::delete(db, id).await?;
    Ok((affected == 1).then(|| ()))
}

// Get a list of IDs for all servers in the database
#[get("/")]
async fn list(db: gameserver_database::Db) -> Result<Json<Vec<Option<i32>>>> {
    let ids: Vec<Option<i32>> = gameserver_database::list(db).await?;
    Ok(Json(ids))
}

// Add a server to the database
#[post("/", format = "application/json", data = "<server>")]
async fn create(
    db: gameserver_database::Db,
    server: Json<Server>,
) -> Result<Created<Json<Server>>, Status> {
    let result = gameserver_database::create(db, server.into_inner()).await;
    match result {
        Ok(result) => Ok(Created::new("/").body(Json(result))),
        Err(_) => Err(Status::InternalServerError)
    }
}

// Destroy the gameservers table
// TEMPORARY FOR DEBUGGING
#[delete("/")]
async fn destroy(db: gameserver_database::Db) -> Result<()> {
    gameserver_database::destroy(db).await?;
    Ok(())
}

// Set up Fairings, run migrations, and handle routes
pub fn stage() -> AdHoc {
    AdHoc::on_ignite("Diesel Postgres Stage", |rocket| async {
        rocket.attach(gameserver_database::Db::fairing()).mount(
            "/",
            routes![list, read, create, delete, destroy, all, update],
        )
    })
}

