use crate::schemas::gameservers;
use rocket::serde::{Deserialize, Serialize};

// Table Type for Create, Get, List operations
#[derive(Debug, Clone, PartialEq, Deserialize, Serialize, Queryable, Insertable)]
#[serde(crate = "rocket::serde")]
#[table_name = "gameservers"]
pub struct Server {
    #[serde(skip_deserializing)]
    pub id: Option<i32>,
    pub name: String,
    pub address: String,
    pub location: String,
    pub description: String,
    pub version: String,
    pub players: i32,
    pub maxplayers: i32,
    pub auth: String,
    pub plugins: String,
    pub whitelisted: bool,
    #[serde(skip_deserializing)]
    pub created: Option<chrono::NaiveDateTime>,
}

// Table Type for Update operation (Allows for name/address changes without breaking other fields)
#[derive(Debug, Clone, PartialEq, Deserialize, Serialize, Queryable, Insertable, AsChangeset)]
#[serde(crate = "rocket::serde")]
#[table_name = "gameservers"]
pub struct UpdateServer {
    #[serde(skip_deserializing)]
    pub id: Option<i32>,
    pub name: Option<String>,
    pub address: Option<String>,
    pub location: Option<String>,
    pub description: Option<String>,
    pub version: Option<String>,
    pub players: Option<i32>,
    pub maxplayers: Option<i32>,
    pub auth: Option<String>,
    pub plugins: Option<String>,
    pub whitelisted: Option<bool>,
}
