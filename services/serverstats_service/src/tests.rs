use crate::models::Server;
use rocket::fairing::AdHoc;
use rocket::http::Status;
use rocket::local::blocking::Client;
use std::fs;

// TODO: Refactor into multiple tests.
fn test(base: &str, stage: AdHoc) {
    // Number of servers we're going to create/read/delete.
    const N: i32 = 200;

    // NOTE: If we had more than one test running concurently that dispatches
    // DB-accessing requests, we'd need transactions or to serialize all tests.

    let client = Client::tracked(rocket::build().attach(stage)).unwrap();

    // Clear everything from the database.
    assert_eq!(client.delete(base).dispatch().status(), Status::Ok);
    assert_eq!(
        client.get(base).dispatch().into_json::<Vec<i64>>(),
        Some(vec![])
    );

    // Add some random servers, ensure they're listable and readable.
    for i in 1..=N {
        let name = format!("server{}", i);
        let address = format!("{}", i);
        let location = format!("location{}", i);
        let description = format!("description{}", i);
        let version = format!("version{}", i);
        let players = i;
        let maxplayers = i + 499;
        let auth = format!("auth{}", i);
        let plugins = format!("pluginA{},pluginB{}", i, i);
        let whitelisted = i == 1;
        let server = Server {
            id: None,
            name,
            address,
            location,
            description,
            version,
            players,
            maxplayers,
            auth,
            plugins,
            whitelisted,
            created: None,
        };

        // Create a new server.
        let response = client
            .post(base)
            .json(&server)
            .dispatch()
            .into_json::<Server>();
        assert_eq!(response.unwrap(), server);

        // Ensure the index shows one more server.
        let list = client.get(base).dispatch().into_json::<Vec<i64>>().unwrap();
        assert_eq!(list.len() as i32, i);

        // The last in the index is the new one; ensure contents match.
        let last = list.last().unwrap();
        let response = client.get(format!("{}/{}", base, last)).dispatch();
        assert_eq!(response.into_json::<Server>().unwrap(), server);
    }

    // Filter for Name (string)
    let (servers,_) = client
        .get(format!("{}all?name=server1", base))
        .dispatch()
        .into_json::<(Vec<Server>, i64)>()
        .unwrap();
    assert_eq!(servers.len(), 1);

    // Filter for Address/Port (string?)
    let (servers,_) = client
        .get(format!("{}all?address=1", base))
        .dispatch()
        .into_json::<(Vec<Server>, i64)>()
        .unwrap();
    assert_eq!(servers.len(), 1);

    // Filter for Location - (string)
    let (servers,_) = client
        .get(format!("{}all?location=location1", base))
        .dispatch()
        .into_json::<(Vec<Server>, i64)>()
        .unwrap();
    assert_eq!(servers.len(), 1);

    // Filter for Version - (string)
    let (servers,_) = client
        .get(format!("{}all?version=version1", base))
        .dispatch()
        .into_json::<(Vec<Server>, i64)>()
        .unwrap();
    assert_eq!(servers.len(), 1);

    // Filter for Players - (i32)
    let (servers,_) = client
        .get(format!("{}all?players=1", base))
        .dispatch()
        .into_json::<(Vec<Server>, i64)>()
        .unwrap();
    assert_eq!(servers.len(), 1);

    // Filter for Max Players - (i32)
    let (servers,_) = client
        .get(format!("{}all?maxplayers=500", base))
        .dispatch()
        .into_json::<(Vec<Server>, i64)>()
        .unwrap();
    assert_eq!(servers.len(), 1);

    // Filter for Auth - (string)
    let (servers,_) = client
        .get(format!("{}all?auth=auth1", base))
        .dispatch()
        .into_json::<(Vec<Server>, i64)>()
        .unwrap();
    assert_eq!(servers.len(), 1);

    // Filter for Plugins - (string)
    let (servers,_) = client
        .get(format!("{}all?plugins=pluginB1,pluginA1", base))
        .dispatch()
        .into_json::<(Vec<Server>, i64)>()
        .unwrap();
    assert_eq!(servers.len(), 1);

    // Filter for whitelisted Server - (bool)
    let (servers,_) = client
        .get(format!("{}all?whitelisted=yes", base))
        .dispatch()
        .into_json::<(Vec<Server>, i64)>()
        .unwrap();
    assert_eq!(servers.len(), 1);

    // Pagination (default pagesize = 10)
    let (servers, pages) = client
        .get(format!("{}all?page=2", base))
        .dispatch()
        .into_json::<(Vec<Server>, i64)>()
        .unwrap();
    assert_eq!(servers.len(), 10);
    assert_eq!(pages, 20);


    // Pagination (default page = 1)
    let (servers, pages) = client
        .get(format!("{}all?pagesize=20", base))
        .dispatch()
        .into_json::<(Vec<Server>, i64)>()
        .unwrap();
    assert_eq!(servers.len(), 20);
    assert_eq!(pages, 10);

    // Pagination
    let (servers, pages) = client
        .get(format!("{}all?page=3&pagesize=10", base))
        .dispatch()
        .into_json::<(Vec<Server>, i64)>()
        .unwrap();
    assert_eq!(servers.len(), 10);
    assert_eq!(pages, 20);

    // Now delete all of the servers.
    for _ in 1..=N {
        // Get a valid ID from the index.
        let list = client.get(base).dispatch().into_json::<Vec<i64>>().unwrap();
        let id = list.get(0).expect("have server");

        // Delete that server.
        let response = client.delete(format!("{}/{}", base, id)).dispatch();
        assert_eq!(response.status(), Status::Ok);
    }

    // Ensure they're all gone.
    let list = client.get(base).dispatch().into_json::<Vec<i64>>().unwrap();
    assert!(list.is_empty());

    // Trying to delete should now 404.
    let response = client.delete(format!("{}/{}", base, 1)).dispatch();
    assert_eq!(response.status(), Status::NotFound);
}

#[test]
fn test_diesel() {
    fs::remove_file("db/diesel/db.sqlite").unwrap_or(warn!("DB Files dont exist. continue"));
    fs::remove_file("db/diesel/db.sqlite-wal").unwrap_or(warn!("DB Files dont exist. continue"));
    fs::remove_file("db/diesel/db.sqlite-shm").unwrap_or(warn!("DB Files dont exist. continue"));
    test("/", crate::gameserver_api::stage())
}
