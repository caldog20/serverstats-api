#[macro_use]
extern crate diesel;
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_sync_db_pools;
#[cfg(test)]
mod tests;

mod gameserver_api;
mod gameserver_database;
mod models;
mod schemas;

mod database_cleaner;

#[tokio::main]
async fn main() {
    match tokio::join!(
        database_cleaner::run(),
        rocket::build().attach(gameserver_api::stage()).launch()
    ) {
        (Err(error), _) => println!("Error occurred during database clean up: {:?}", error),
        (_, Err(error)) => println!("Error occurred during server execution: {:?}", error),
        _ => {},
    }
}
