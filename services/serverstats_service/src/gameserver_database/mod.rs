use self::diesel::prelude::*;
use crate::models::{Server, UpdateServer};
use crate::schemas::gameservers;

mod pagination;

use self::pagination::*;

use rocket_sync_db_pools::diesel;

#[database("diesel")]
pub struct Db(diesel::pg::PgConnection);

#[derive(FromForm)]
pub struct QueryParameters {
    pub name: Option<String>,
    pub address: Option<String>,
    pub location: Option<String>,
    pub version: Option<String>,
    pub players: Option<i32>,
    pub maxplayers: Option<i32>,
    pub auth: Option<String>,
    pub plugins: Option<String>,
    pub whitelisted: Option<bool>,
    pub pagesize: Option<i64>,
    pub page: Option<i64>,
}

no_arg_sql_function!(
    last_insert_rowid,
    diesel::sql_types::Integer,
    "Represents the SQL last_insert_row() function"
);

// Add a server to the database
pub async fn create(db: Db, post: Server) -> QueryResult<Server> {
    // let post_value = post.clone();
    db.run(move |conn| {
        diesel::insert_into(gameservers::table)
            .values(&post)
            .get_result(conn)
    })
    .await
}

// Update fields on a server already existing in the database
// TODO: Vefiy proper result return on errors
pub async fn update(db: Db, id: i32, post: UpdateServer) -> Result<usize, diesel::result::Error> {
    let post_value = post.clone();
    db.run(move |conn| {
        diesel::update(gameservers::table.find(id))
            .set(post_value)
            .execute(conn)
    })
    .await
}

// Get a list of IDs for all servers in the database
pub async fn list(db: Db) -> Result<Vec<Option<i32>>, diesel::result::Error> {
    db.run(move |conn| gameservers::table.select(gameservers::id).load(conn))
        .await
}

// Get a full list of all Servers (possibly filtered) with fields
pub async fn all(db: Db, params: QueryParameters) -> Result<(Vec<Server>, i64), diesel::result::Error> {
    db.run(move |conn| {

        let mut query = gameservers::table.into_boxed();

        // Filter for name
        if let Some(name) = params.name {
            if !name.is_empty() {
                query = query.filter(gameservers::name.eq(name));
            }
        }

        // Filter for address
        if let Some(address) = params.address {
            if !address.is_empty() {
                query = query.filter(gameservers::address.eq(address));
            }
        }

        // Filter for location 
        if let Some(location) = params.location {
            if !location.is_empty() {
                query = query.filter(gameservers::location.eq(location));
            }
        }

        // Filter for version 
        if let Some(version) = params.version {
            if !version.is_empty() {
                query = query.filter(gameservers::version.eq(version));
            }
        }

        // Filter for players 
        if let Some(players) = params.players {
            query = query.filter(gameservers::players.le(players));
        }

        // Filter for maxplayers 
        if let Some(maxplayers) = params.maxplayers {
            query = query.filter(gameservers::maxplayers.le(maxplayers));
        }

        // Filter for auth 
        if let Some(auth) = params.auth {
            if !auth.is_empty() {
                query = query.filter(gameservers::auth.eq(auth));
            }
        }

        // Filter for plugins 
        if let Some(plugins) = params.plugins {
            if !plugins.is_empty() {
                let mut plugins = plugins.split(",").collect::<Vec<&str>>();
                plugins.sort();
                query = query.filter(gameservers::plugins.eq(plugins.join(",")));
            }
        }

        // Filter for whitelisted 
        if let Some(whitelisted) = params.whitelisted {
            query = query.filter(gameservers::whitelisted.eq(whitelisted));
        }

        // Set page according to params or default to 1
        let page = match params.page {
            Some(p) => p,
            None => 1
        };

        // Set pagesize according to params or default to 10
        let pagesize = match params.pagesize {
            Some(p) => p,
            None => 10
        };

        // Paginate
        let query = query.paginate(page).per_page(pagesize);
        
        match query.load_and_count_pages::<Server>(&conn) {
            Ok((servers, pages)) => Ok((servers, pages)),
            Err(err) => Err(err),
        }

    }).await
}

// Get a specific server with fields
pub async fn read(db: Db, id: i32) -> Result<Server, diesel::result::Error> {
    db.run(move |conn| {
        gameservers::table
            .filter(gameservers::id.eq(id))
            .first(conn)
    })
    .await
}

// Delete a specific server by ID
pub async fn delete(db: Db, id: i32) -> Result<usize, diesel::result::Error> {
    db.run(move |conn| {
        diesel::delete(gameservers::table)
            .filter(gameservers::id.eq(id))
            .execute(conn)
    })
    .await
}

// Delete a set of servers by IDs
pub async fn delete_vec(db: Db, ids: Vec<i32>) -> Result<usize, diesel::result::Error> {
    db.run(move |conn| {
        diesel::delete(gameservers::table)
            .filter(gameservers::id.eq_any(ids))
            .execute(conn)
    })
    .await
}

// Destroy the gameservers table
// TEMPORARY FOR DEBUGGING
pub async fn destroy(db: Db) -> Result<usize, diesel::result::Error> {
    db.run(move |conn| diesel::delete(gameservers::table).execute(conn))
        .await
}
