table! {
    gameservers (id) {
        id -> Nullable<Integer>,
        name -> Text,
        address -> Text,
        location -> Text,
        description -> Text,
        version -> Text,
        players -> Integer,
        maxplayers -> Integer,
        auth -> Text,
        plugins -> Text,
        whitelisted -> Bool,
        created -> Nullable<Timestamp>,
    }
}
