use diesel::pg::PgConnection;
use diesel::r2d2::ConnectionManager;
use tokio::task;
use tokio::time::{self, Duration};

use crate::diesel::query_dsl::filter_dsl::FilterDsl;
use crate::diesel::{ExpressionMethods, RunQueryDsl};
use crate::schemas::gameservers::dsl::{created, gameservers};

fn get_connection_pool() -> diesel::r2d2::Pool<ConnectionManager<PgConnection>> {
    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");

    let manager = ConnectionManager::<PgConnection>::new(database_url);
    diesel::r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create postgres pool.")
}

fn remove_stale_entries(conn: &PgConnection, max_lifetime: i64) -> std::result::Result<(), diesel::result::Error> {
    let now = chrono::Utc::now().naive_utc();
    match diesel::delete(
        gameservers.filter(created.lt(now - chrono::Duration::seconds(max_lifetime))),
    )
    .execute(conn)
    {
        Ok(cleaned_up) => {
            println!("Removed {} entries", cleaned_up);
            Ok(())
        },
        Err(err) => Err(err)
    }
}

// TODO: Use BTreeMap or something similar to be smarter about how long to wait
//       between cleanup iterations.
//       Inspiration: https://github.com/tokio-rs/mini-redis/blob/master/src/db.rs
async fn start_cleanup() -> std::result::Result<(), diesel::result::Error> {
    let pool = get_connection_pool();

    let mut interval = time::interval(Duration::from_secs(300));
    loop {
        interval.tick().await;
        let conn = pool.get().unwrap();
        remove_stale_entries(&conn, 300)?;
    }
}

pub async fn run() -> Result<(), diesel::result::Error> {
    match task::spawn(start_cleanup()).await {
        Ok(Err(error)) => Err(error),
        Ok(_) => Ok(()),
        Err(error) => {
            println!("Failed to execute task to completion: {:?}", error);
            Ok(())
        }
    }
}
