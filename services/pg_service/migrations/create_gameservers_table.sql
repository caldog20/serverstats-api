CREATE TABLE IF NOT EXISTS gameservers (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    address VARCHAR NOT NULL,
    location VARCHAR NOT NULL,
    description TEXT,
    version TEXT,
    players INTEGER,
    maxplayers INTEGER,
    auth TEXT,
    plugins TEXT,
    whitelisted BOOLEAN NOT NULL DEFAULT '0',
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
